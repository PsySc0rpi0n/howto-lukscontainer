#!/bin/bash

function open_secure_vault(){
   (
      trap '' HUP INT QUIT TERM && gpg2 -v --decrypt "$VAULT_KEY"  2>/dev/null |
         sudo cryptsetup -v --key-file=- luksOpen "$CONTAINER_FILE" "$CONTAINER_NAME"
   )
   (
      trap '' HUP INT QUIT TERM && sudo qemu-system-x86_64 -cpu host -enable-kvm \
         -smp 4 -m 8192 -drive file=/dev/mapper/"$CONTAINER_NAME",if=virtio,format=raw
   )
}

function check_open_vault(){
   if [[ -L "/dev/mapper/$CONTAINER_NAME" ]]; then
      (
         trap '' HUP INT QUIT TERM && sudo cryptsetup luksClose "$CONTAINER_NAME"
      )
      printf "Closed open vault\n"
      return 1
   fi
   printf "All good.\n"
   return 0
}

function show_menu(){
   while true; do
      printf "Bitcoin Secure Vault\n"
      printf "Choose an option:\n"
      printf "\t[O]pen Secure Vault\n"
      printf "\t[C]lose Secure Vault\n"
      printf "\t[E]xit\n"
      while read -r -p "> " OPT; do
         case $OPT in
            o | O)
               printf "\nopen_secure_vault\n"
               open_secure_vault
               ;;
            c | C)
               printf "\nToDo probably not needed!\n"
               ;;
            e | E)
               printf "\nSee you next time, Bitcoiner!\n"
               exit
               ;;
            ?) printf "\n%s is not a recognized option!\n" "$OPT"
               show_help
               ;;
         esac
      done
   done
}

function check_params(){
   if [[ $1 -lt 6 ]]; then
      show_help
      exit
   fi
}

function show_help(){
   printf "%sHelp menu for Secure Vault%s\n" "---" "---"
   printf "Usage: %s [ -e <encryption file> ] [ -c <container file> ] [ -n <container name> ]\n" "$SCRIPT_NAME"
}

function parse_options(){
   while getopts ':e:c:n:' OPT; do
      case "$OPT" in
         e)
            VAULT_KEY="$OPTARG"
            printf "Vault key: %s\n" "$OPTARG" 
            ;;
         c)
            CONTAINER_FILE="$OPTARG"
            printf "Container file: %s\n" "$OPTARG"
            ;;
         n)
            CONTAINER_NAME="$OPTARG"
            printf "Container name: %s\n" "$OPTARG"
            ;;
         ?)
            show_help
            exit 1
            ;;
      esac
   done
}

function signal_warn(){
   printf "\nDon't use CTRL+C. Vault might be left decrypted!"
   printf "\nPress Enter!\n"
}

#-- Script body starts here --#
set -x
trap '' HUP INT QUIT TERM EXIT
check_params "$#"
SCRIPT_NAME="$0"
parse_options "$@"
check_open_vault
show_menu
trap - HUP INT QUIT TERM EXIT
