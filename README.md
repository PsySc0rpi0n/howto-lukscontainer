**Instructions to create a small secure container**

Tools used:

    - gpg2

    - dd

    - mkfs

    - cryptsetup (LUKS)


**Step 1**

    - Install GnuPG2

**Step 2**

    - Install Cryptsetup for LUKS encryption


**Step 3** (only when creating the volume)

    - Create a key file to encrypt the volume, using /dev/urandom device and   \
        encrypt it using GnuPG2 but without ever saving the key in any media   \
        other than volatile memory (ramfs). To do so, we send the command      \
         output to the void (/dev/null) and pipe the key output directly into  \
        GnuPG2 for direct encryption. The final result will be redirected to   \
        the key file. The key will be 4096 bytes long.

    Command: $>dd if=/dev/urandom bs=512 count=8 iflag=fullblock | gpg2 -v      \
                --cipher-algo aes256 --digest-algo sha512 -c -a > vault-key.gpg
    
**Step 4** (only when creating the volume)

    - Create a sparse file which will be the container/volume
        A sparse file is a type of file that has some space reserved to it but \
        actually, it will only use as much space as its content uses.
        Let's say we create a 256MB size file and store 25MB inside it. Then   \
        this sparse file will actually use only 25MB of space in your hard drive

    Command: $>dd if=/dev/zero bs=1 count=0 seek=30G of=vault-container.ctn

**Step 5** (only when creating the volume)

    - Create, format and encrypt the volume with the previously created key.
        With the redirection after the gpg2 command, we ensure the output of
        the gpg2 command is never saved and is destroyed as soon as it has been\
        pipped to `cryptsetup`.

    Command: $> gpg2 --decrypt vault-key.gpg 2>/dev/null | sudo cryptsetup\
                -v --key-file=- -c aes-xts-plain64 -s 512 -h sha512 luksFormat \
                vault-container.ctn

**Step 6**

    - Now we can open the encrypted volume so that we can use it.

    Command: $> gpg2 -v --decrypt vault-key.key 2>/dev/null | sudo        \
                cryptsetup -v --key-file=- luksOpen vault-container.ctn vaultContainer

**Step 7** (only when creating the volume)

    - Now we can format the volume to the file system of your choice. I chose   \
        btrfs because seems it offers some more stability.

    Command: $> sudo mkfs.btrfs /dev/mapper/vaultContainer

**Step 8** (only when installing the OS)

    - Install Debian

    Comman: $> sudo qemu-system-x86_64 -machine accel=kvm:tcg -m 8192 \
        -drive format=raw,node-name=vaultContainer,file.driver=host_device,file.filename=/dev/mapper/vaultContainer\
        -boot d -cdrom ~/Storage/Software/Linux/LinuxIMGsPool/debian-image.iso

**Step 9**

    - Start tthe secure VM from within the Luks container

    Command: $> sudo qemu-system-x86_64 -cpu host -enable-kvm -smp 4 -m 8192 \
        -drive file=/dev/mapper/vaultContainer,if=virtio,format=raw

**To close and secure the volume:**


**Setp 10**

    - Close the LUKS volume. This encrypts back the volume and secures it.

    Command: $> sudo cryptsetup luksClose vaultContainer


**To create a shared folder to transfer files between host and guest machines**

First, on the host machine

**Step 1**

    - Create the raw image (only when creating the image)

    Command: $> dd if=/dev/urandom bs=1k count=10 of=./image.raw

**Step 2**

    - Format the image (only when creating the image)

    Command: $> sudo mkfs.ext4 image.raw

**Setp 3**

    - Mount the image (only for adding or removing files to be transferred)

    Command: $> sudo mount -o rw image.raw /dev/path-to-mount-point

Then, on the guest machine,read section above to start the Secure VM, but use the following command to start

the Secure VM with the extra drive/folder

**Step 4**

    - Start the Secure VM with the extra folder

    Command: $> sudo qemu-system-x86_64 -cpu host -enable-kvm -smp 4 \
                    -m 8192 -drive file=/dev/mapper/vaultContainer,if=virtio,format=raw \
                    -drive file=./image.raw,if=virtio


**Step 5**

    - Check if the said folder is available from the Secure VM
    
    Command: $> sudo blkid

        and check for maybe a `vdb` device.

**Step 6**

    - Mount the device from the previous step in some mount point

    Command: $> sudo mount -o loop,rw /dev/vdb /path/to/mount/point

**Step 7**

    - Copy the needed files over to the guest machine and/or from the guest machine to the/from the host machine

**Step 8**

    - Unmount the `vdb` device in the guet machine
    
    Command: $> sudo umount /path/to/mount/point

**Step 9**

    - Do whatever else is needed and power off the guest machine

    Command: $> sudo poweroff


Resources used:\
    GnuPG man pages\
    cryptsetup man pages\
    qemu man pages\

Online resources used:\
    https://wiki.archlinux.org/index.php/Sparse_file\
    https://linuxconfig.org/basic-guide-to-encrypting-linux-partitions-with-luks\
    https://bbs.archlinux.org/viewtopic.php?id=120243\
